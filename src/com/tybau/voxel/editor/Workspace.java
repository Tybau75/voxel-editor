package com.tybau.voxel.editor;

import static org.lwjgl.opengl.GL11.*;

import javax.swing.JOptionPane;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.tybau.voxel.Editor;
import com.tybau.voxel.editor.files.Export;
import com.tybau.voxel.editor.menu.Menu;
import com.tybau.voxel.inputs.MouseState;
import com.tybau.voxel.maths.Vec3;
import com.tybau.voxel.render.Camera;
import com.tybau.voxel.render.Color4;

import fr.veridiangames.core.PerlinNoise;

/**
 * Created by Tybau on 18/01/2016.
 */
public class Workspace extends Menu
{
	Camera cam;
	private int distance;
	private boolean mode;

	private Voxel[][][] voxels;
	private HUD hud;

	private int size;

	public Workspace()
	{
		int size = Integer.parseInt(JOptionPane.showInputDialog("Size:"));
		
		cam = new Camera(new Vec3(8, 8, -5));
		cam.setPerspectiveProjection(70.0f, 0.1f, 100.0f);

		this.size = size;

		voxels = new Voxel[size][size][size];
		distance = 3;
		mode = false;

		hud = new HUD();
	}
	
	public Workspace(Voxel[][][] voxels)
	{
		cam = new Camera(new Vec3(8, 8, -5));
		cam.setPerspectiveProjection(70.0f, 0.1f, 100.0f);

		this.size = voxels.length;

		this.voxels = voxels;
		distance = 3;

		hud = new HUD();
	}

	public void update()
	{
		float w = Mouse.getDWheel();

		if (w < 0 && distance > 2)
		{
			distance--;
		} else if (w > 0 && distance < 10)
		{
			distance++;
		}

		Vec3 ray = getRayCast();

		int x = (int) ray.x;
		int y = (int) ray.y;
		int z = (int) ray.z;

		if (MouseState.isLeftClickPressed() && x >= 0 && x < size && y >= 0 && y < size && z >= 0 && z < size
				&& Mouse.isGrabbed())
		{
			voxels[x][y][z] = new Voxel(ray, hud.color, hud.solid, hud.gravity, hud.light, hud.construct);
		}

		if (MouseState.isRightClickPressed() && x >= 0 && x < size && y >= 0 && y < size && z >= 0 && z < size
				&& Mouse.isGrabbed())
		{
			voxels[x][y][z] = null;
		}

		if (Mouse.isButtonDown(2) && x >= 0 && x < size && y >= 0 && y < size && z >= 0 && z < size
				&& Mouse.isGrabbed())
		{
			if (voxels[x][y][z] != null)
			{
				hud.color = new Color4(voxels[x][y][z].getColor());
				hud.solid = voxels[x][y][z].isSolid();
				hud.gravity = voxels[x][y][z].isGravity();
				hud.light = voxels[x][y][z].isLight();
				hud.construct = voxels[x][y][z].isConstruct();
			}
		}

		hud.update();

		if (hud.getGenButton().isPressed()) genTerrain();
		
		if (hud.getClearButton().isPressed()) clearConstruct();
		
		if (hud.getSaveButton().isPressed()) Export.toVOBJ(voxels);
		
		if (Keyboard.isKeyDown(Keyboard.KEY_TAB)) mode = true;
		else mode = false;

		if (Mouse.isGrabbed())
			cam.input();
	}

	public void render()
	{
		cam.getPerspectiveProjection();
		cam.update();

		spaceRender();

		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				for (int z = 0; z < size; z++)
				{
					if (voxels[x][y][z] != null)
						voxels[x][y][z].render();
				}
			}
		}

		select();
	}

	public void renderGui()
	{
		hud.render();
	}

	private void spaceRender()
	{
		Color4.RED.bind();
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glBegin(GL_QUADS);
		glVertex3f(0, 0, 0);
		glVertex3f(size, 0, 0);
		glVertex3f(size, size, 0);
		glVertex3f(0, size, 0);

		glVertex3f(0, 0, size);
		glVertex3f(size, 0, size);
		glVertex3f(size, size, size);
		glVertex3f(0, size, size);

		glVertex3f(0, 0, 0);
		glVertex3f(0, size, 0);
		glVertex3f(0, size, size);
		glVertex3f(0, 0, size);

		glVertex3f(size, 0, 0);
		glVertex3f(size, size, 0);
		glVertex3f(size, size, size);
		glVertex3f(size, 0, size);

		glVertex3f(0, 0, 0);
		glVertex3f(size, 0, 0);
		glVertex3f(size, 0, size);
		glVertex3f(0, 0, size);

		glVertex3f(0, size, 0);
		glVertex3f(size, size, 0);
		glVertex3f(size, size, size);
		glVertex3f(0, size, size);
		glEnd();
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		Color4.unbind();
	}

	public Voxel getVoxel(int x, int y, int z)
	{
		return voxels[x][y][z];
	}

	public void setVoxel(Voxel vox, int x, int y, int z)
	{
		voxels[x][y][z] = vox;
	}

	public Vec3 getRayCast()
	{
		Vec3 dir = cam.getDefaultDirection();
		
		int x = -1, y = -1, z = -1;
		
		if(!mode){
			x = (int) (cam.getPos().x + dir.x * distance);
			y = (int) (cam.getPos().y + dir.y * distance);
			z = (int) (cam.getPos().z + dir.z * distance);
			return new Vec3(x, y, z);
		}
		else
		{
			for(int i = 0; i < size; i++)
			{
				x = (int) (cam.getPos().x + dir.x * i);
				y = (int) (cam.getPos().y + dir.y * i);
				z = (int) (cam.getPos().z + dir.z * i);
				
				if (x >= 0 && x < size && y >= 0 && y < size && z >= 0 && z < size && voxels[x][y][z] != null)
				{
					x = (int) (cam.getPos().x + dir.x * (i - 1));
					y = (int) (cam.getPos().y + dir.y * (i - 1));
					z = (int) (cam.getPos().z + dir.z * (i - 1));
					return new Vec3(x, y, z);
				}
			}
		}

		return new Vec3(-1, -1, -1);
	}

	private void select()
	{
		Vec3 ray = getRayCast();

		int x = (int) ray.x;
		int y = (int) ray.y;
		int z = (int) ray.z;

		if (x >= 0 && x < size && y >= 0 && y < size && z >= 0 && z < size)
		{
			int s = 1;

			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			glBegin(GL_QUADS);
			glVertex3f(x, y, z);
			glVertex3f(x + s, y, z);
			glVertex3f(x + s, y + s, z);
			glVertex3f(x, y + s, z);

			glVertex3f(x, y, z + s);
			glVertex3f(x + s, y, z + s);
			glVertex3f(x + s, y + s, z + s);
			glVertex3f(x, y + s, z + s);

			glVertex3f(x, y, z);
			glVertex3f(x, y + s, z);
			glVertex3f(x, y + s, z + s);
			glVertex3f(x, y, z + s);

			glVertex3f(x + s, y, z);
			glVertex3f(x + s, y + s, z);
			glVertex3f(x + s, y + s, z + s);
			glVertex3f(x + s, y, z + s);

			glVertex3f(x, y, z);
			glVertex3f(x + s, y, z);
			glVertex3f(x + s, y, z + s);
			glVertex3f(x, y, z + s);

			glVertex3f(x, y + s, z);
			glVertex3f(x + s, y + s, z);
			glVertex3f(x + s, y + s, z + s);
			glVertex3f(x, y + s, z + s);
			glEnd();
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
	}

	private void genTerrain()
	{
		PerlinNoise noise = new PerlinNoise(Editor.RANDOM.nextLong(), 5, 5);

		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				for (int z = 0; z < size; z++)
				{
					if (y < noise.getNoise(x, z))
						voxels[x][y][z] = new Voxel(new Vec3(x, y, z), hud.color, hud.solid, hud.gravity, hud.light,
								hud.construct);
				}
			}
		}
	}

	private void clearConstruct()
	{
		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				for (int z = 0; z < size; z++)
				{
					if (voxels[x][y][z] != null && voxels[x][y][z].isConstruct())
						voxels[x][y][z] = null;
				}
			}
		}
	}
}
