package com.tybau.voxel.editor;

import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glRectf;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import com.tybau.voxel.maths.Vec2;
import com.tybau.voxel.render.Color4;
import com.tybau.voxel.render.guis.Button;
import com.tybau.voxel.render.guis.Checkbox;

/**
 * Created by Tybau on 19/01/2016.
 */
public class HUD
{
	public Color4 color;
	public boolean solid;
	public boolean gravity;
	public boolean light;
	public boolean construct;

	Button upRed, downRed;
	Button upGreen, downGreen;
	Button upBlue, downBlue;
	Button upAlpha, downAlpha;

	Checkbox solidBox;
	Checkbox gravityBox;
	Checkbox lightBox;
	Checkbox constructBox;

	Button gen;
	Button clear;
	Button save;

	Button build;

	int r, g, b, a;

	public HUD()
	{
		solid = true;
		gravity = false;
		light = false;
		construct = false;

		color = Color4.WHITE;

		downRed = new Button("<", new Vec2(10, 10));
		upRed = new Button(">", new Vec2(100, 10));

		downGreen = new Button("<", new Vec2(10, 40));
		upGreen = new Button(">", new Vec2(100, 40));

		downBlue = new Button("<", new Vec2(10, 70));
		upBlue = new Button(">", new Vec2(100, 70));

		downAlpha = new Button("<", new Vec2(10, 100));
		upAlpha = new Button(">", new Vec2(100, 100));

		solidBox = new Checkbox(new Vec2(10, 220), solid, "Solid");
		gravityBox = new Checkbox(new Vec2(10, 250), gravity, "Allow gravity");
		lightBox = new Checkbox(new Vec2(10, 280), light, "Light");
		constructBox = new Checkbox(new Vec2(10, 310), construct, "Construct");

		gen = new Button("Terrain", new Vec2(10, 350));
		clear = new Button("Clear", new Vec2(10, 380));
		save = new Button("Save", new Vec2(10, 440));

		build = new Button("Build", new Vec2(Display.getWidth() - 100, Display.getHeight() - 50));

		r = 255;
		g = 255;
		b = 255;
		a = 255;
	}

	public void update()
	{
		r = (int) (color.r * 255);
		g = (int) (color.g * 255);
		b = (int) (color.b * 255);
		a = (int) (color.a * 255);

		solidBox.setChecked(solid);
		gravityBox.setChecked(gravity);
		lightBox.setChecked(light);
		constructBox.setChecked(construct);

		if (downRed.isDown() && r > 0)
			r--;
		if (upRed.isDown() && r < 255)
			r++;

		if (downGreen.isDown() && g > 0)
			g--;
		if (upGreen.isDown() && g < 255)
			g++;

		if (downBlue.isDown() && b > 0)
			b--;
		if (upBlue.isDown() && b < 255)
			b++;

		if (downAlpha.isDown() && a > 0)
			a--;
		if (upAlpha.isDown() && a < 255)
			a++;

		if (solidBox.isPressed())
			solid = solidBox.isChecked();
		if (gravityBox.isPressed())
			gravity = gravityBox.isChecked();
		if (lightBox.isPressed())
			light = lightBox.isChecked();
		if (constructBox.isPressed())
			construct = constructBox.isChecked();

		if (build.isPressed())
			Mouse.setGrabbed(true);

		color.r = (float) r / 255.0f;
		color.g = (float) g / 255.0f;
		color.b = (float) b / 255.0f;
		color.a = (float) a / 255.0f;
	}

	public void render()
	{
		downRed.render();
		upRed.render();

		glColor4f((float) r / 255.0f, 0, 0, 0.7f);
		glRectf(23, 5, 95, 30);
		Color4.unbind();

		downGreen.render();
		upGreen.render();

		glColor4f(0, (float) g / 255.0f, 0, 0.7f);
		glRectf(23, 35, 95, 60);
		Color4.unbind();

		downBlue.render();
		upBlue.render();

		glColor4f(0, 0, (float) b / 255.0f, 0.7f);
		glRectf(23, 65, 95, 90);
		Color4.unbind();

		downAlpha.render();
		upAlpha.render();

		glColor4f((float) a / 255.0f, (float) a / 255.0f, (float) a / 255.0f, 0.7f);
		glRectf(23, 95, 95, 120);
		Color4.unbind();

		color.bind();
		glRectf(5, 130, 80, 205);
		Color4.unbind();

		solidBox.render();
		gravityBox.render();
		lightBox.render();
		constructBox.render();

		gen.render();
		clear.render();
		save.render();

		if (!Mouse.isGrabbed())
			build.render();
	}

	public Button getGenButton()
	{
		return gen;
	}

	public Button getClearButton()
	{
		return clear;
	}
	
	public Button getSaveButton()
	{
		return save;
	}
}
