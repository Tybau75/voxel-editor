package com.tybau.voxel.editor.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JOptionPane;

import com.tybau.voxel.editor.Voxel;
import com.tybau.voxel.maths.Vec3;
import com.tybau.voxel.render.Color4;

/**
 * Created by Tybau on 20/01/2016.
 */
public class Import
{
	public static Voxel[][][] fromVOBJ(){
		
		String name = JOptionPane.showInputDialog("Path:") + ".vobj";
		int size = 16;
		
		File file = new File(name);
		if(!file.exists()) return new Voxel[16][16][16];
		
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(file));
		}
		catch (FileNotFoundException e1)
		{
			e1.printStackTrace();
		}
		
        String line;
        
        Voxel[][][] voxels = null;
        
        try {
			while((line = br.readLine()) != null){
				
				if(line.startsWith("-"))
				{
					String[] args = line.split(" ");
					if(args[0].equalsIgnoreCase("-size"))
					{
						size = Integer.parseInt(args[1]);
						voxels = new Voxel[size][size][size];
					}
					
					if(args[0].equalsIgnoreCase("-vox"))
					{
						String[] coords = args[1].split(",");
						String[] c = args[2].split(",");
						
						Vec3 pos = new Vec3(Float.parseFloat(coords[0]), Float.parseFloat(coords[1]), Float.parseFloat(coords[2]));
						Color4 color = new Color4(Float.parseFloat(c[0]), Float.parseFloat(c[1]), Float.parseFloat(c[2]), Float.parseFloat(c[3]));
						
						boolean solid = false;
						boolean gravity = false;
						boolean light = false;
						boolean construct = false;
						
						if(args[3].contains("s")) solid = true;
						if(args[3].contains("g")) gravity = true;
						if(args[3].contains("l")) light = true;
						if(args[3].contains("c")) construct = true;
						
						voxels[(int)pos.x][(int)pos.y][(int)pos.z] = new Voxel(pos, color, solid, gravity, light, construct);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        return voxels;
	}
}
