package com.tybau.voxel.editor.files;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

import com.tybau.voxel.editor.Voxel;

/**
 * Created by Tybau on 20/01/2016.
 */
public class Export
{
	public static void toVOBJ(Voxel[][][] voxels)
	{		
		String name = JOptionPane.showInputDialog("Path:") + ".vobj";
		
		int size = voxels.length;
		
		File file = new File(name);
		
		try
		{
			file.createNewFile();
			FileWriter fw = new FileWriter(file);
			
			fw.write(" _____         _                   \n");
			fw.write("|_   _|       | |                  \n");
			fw.write("  | |   _   _ | |__    __ _  _   _ \n");
			fw.write("  | |  | | | || '_ \\  / _` || | | |\n");
			fw.write("  | |  | |_| || |_) || (_| || |_| |\n");
			fw.write("  \\_/   \\__, ||_.__/  \\__,_| \\__,_|\n");
			fw.write("         __/ |                     \n");
			fw.write("        |___/                      \n\n");
			
			fw.write("-size " + size + "\n\n");
			
			for(int x = 0; x < size; x++)
			{
				for(int y = 0; y < size; y++)
				{
					for(int z = 0; z < size; z++)
					{
						Voxel v = voxels[x][y][z];
						String args = "-";
						if(v != null)
						{
							if(v.isSolid()) args += "s";
							if(v.isGravity()) args += "g";
							if(v.isLight()) args += "l";
							if(v.isConstruct()) args += "c";
							
							fw.write("-vox " + v.getPos().x + "," + v.getPos().y + "," + v.getPos().z + " " +
									v.getColor().r + "," + v.getColor().g + "," + v.getColor().b + "," + v.getColor().a + " " + args + "\n");
							
						}
					}
				}
			}
			
			fw.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
