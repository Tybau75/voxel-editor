package com.tybau.voxel.editor;

import static org.lwjgl.opengl.GL11.*;

import com.tybau.voxel.maths.Vec3;
import com.tybau.voxel.render.Color4;

/**
* Created by Tybau on 18/01/2016.
*/
public class Voxel
{
	private Vec3 pos;
	private Color4 color;
	private boolean solid;
	private boolean gravity;
	private boolean light;
	private boolean construct;
	
	public Voxel(Vec3 pos, Color4 color, boolean solid, boolean gravity, boolean light, boolean construct)
	{
		this.pos = pos;
		this.color = new Color4(color);
		this.solid = solid;
		this.gravity = gravity;
		this.light = light;
		this.construct = construct;
	}
	
	public Voxel(Vec3 pos, Color4 color)
	{
		this(pos, color, true, false, false, false);
	}

	public void render()
	{
		float x = pos.x;
		float y = pos.y;
		float z = pos.z;
		float s = 1;
//		if(construct)
//		{
//			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//			new Color4(new Color4(1, 0, 1)).bind();
//			
//			glBegin(GL_QUADS);
//				glVertex3f(x, y, z);
//				glVertex3f(x + s, y, z);
//				glVertex3f(x + s, y + s, z);
//				glVertex3f(x, y + s, z);
//				
//				glVertex3f(x, y, z + s);
//				glVertex3f(x + s, y, z + s);
//				glVertex3f(x + s, y + s, z + s);
//				glVertex3f(x, y + s, z + s);
//				
//				glVertex3f(x, y, z);
//				glVertex3f(x, y + s, z);
//				glVertex3f(x, y + s, z + s);
//				glVertex3f(x, y, z + s);
//				
//				glVertex3f(x + s, y, z);
//				glVertex3f(x + s, y + s, z);
//				glVertex3f(x + s, y + s, z + s);
//				glVertex3f(x + s, y, z + s);
//				
//				glVertex3f(x, y, z);
//				glVertex3f(x + s, y, z);
//				glVertex3f(x + s, y, z + s);
//				glVertex3f(x, y, z + s);
//				
//				glVertex3f(x, y + s, z);
//				glVertex3f(x + s, y + s, z);
//				glVertex3f(x + s, y + s, z + s);
//				glVertex3f(x, y + s, z + s);
//			glEnd();
//			Color4.unbind();
//			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//		}
		glBegin(GL_QUADS);
			color.bind(0.8f);
			glVertex3f(x, y, z);
			glVertex3f(x + s, y, z);
			glVertex3f(x + s, y + s, z);
			glVertex3f(x, y + s, z);
			
			glVertex3f(x, y, z + s);
			glVertex3f(x + s, y, z + s);
			glVertex3f(x + s, y + s, z + s);
			glVertex3f(x, y + s, z + s);
			
			color.bind(0.7f);
			
			glVertex3f(x, y, z);
			glVertex3f(x, y + s, z);
			glVertex3f(x, y + s, z + s);
			glVertex3f(x, y, z + s);
			
			glVertex3f(x + s, y, z);
			glVertex3f(x + s, y + s, z);
			glVertex3f(x + s, y + s, z + s);
			glVertex3f(x + s, y, z + s);
			
			
			color.bind(0.5f);
			
			glVertex3f(x, y, z);
			glVertex3f(x + s, y, z);
			glVertex3f(x + s, y, z + s);
			glVertex3f(x, y, z + s);
			
			color.bind();
			
			glVertex3f(x, y + s, z);
			glVertex3f(x + s, y + s, z);
			glVertex3f(x + s, y + s, z + s);
			glVertex3f(x, y + s, z + s);
		glEnd();
		Color4.unbind();
		if(construct) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	
	public Vec3 getPos()
	{
		return pos;
	}

	public void setPos(Vec3 pos)
	{
		this.pos = pos;
	}

	public Color4 getColor()
	{
		return color;
	}

	public void setColor(Color4 color)
	{
		this.color = color;
	}

	public boolean isSolid()
	{
		return solid;
	}

	public void setSolid(boolean solid)
	{
		this.solid = solid;
	}

	public boolean isGravity()
	{
		return gravity;
	}

	public void setGravity(boolean gravity)
	{
		this.gravity = gravity;
	}
	
	public boolean isLight()
	{
		return light;
	}

	public void setLight(boolean light)
	{
		this.light = light;
	}

	public boolean isConstruct()
	{
		return construct;
	}

	public void setConstruct(boolean construct)
	{
		this.construct = construct;
	}
}
