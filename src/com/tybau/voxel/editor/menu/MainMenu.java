package com.tybau.voxel.editor.menu;

import com.tybau.voxel.Editor;
import com.tybau.voxel.editor.Workspace;
import com.tybau.voxel.editor.files.Import;
import com.tybau.voxel.maths.Vec2;
import com.tybau.voxel.render.Texture;
import com.tybau.voxel.render.guis.Button;
import com.tybau.voxel.render.guis.Text;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.Display;

/**
 * Created by Tybau on 20/01/2016.
 */
public class MainMenu extends Menu
{
	public static final Texture BACKGROUND = new Texture("background.png");
	
	private Button spaceButton, importButton, infoButton;
	
	public MainMenu()
	{
		spaceButton = new Button("New", new Vec2(Display.getWidth() / 2 - (Text.PIXEL.getTextWidth("New") + 10) / 2, 200));
		importButton = new Button("Import", new Vec2(Display.getWidth() / 2 - (Text.PIXEL.getTextWidth("Import") + 10) / 2, 250));
		infoButton = new Button("Infos", new Vec2(Display.getWidth() / 2 - (Text.PIXEL.getTextWidth("Infos") + 10) / 2, 300));
	}
	
	public void update()
	{
		if(spaceButton.isPressed()) Editor.CURRENT_MENU = new Workspace();
		if(importButton.isPressed()) Editor.CURRENT_MENU = new Workspace(Import.fromVOBJ());
		if(infoButton.isPressed())Editor.CURRENT_MENU = new InfoMenu();
	}

	public void render()
	{
		
	}

	public void renderGui()
	{
		BACKGROUND.bind();
		glBegin(GL_QUADS);
			glTexCoord2f(0, 0);		glVertex2f(0, 0);
			glTexCoord2f(1, 0);		glVertex2f(Display.getWidth(), 0);
			glTexCoord2f(1, 1);		glVertex2f(Display.getWidth(), Display.getHeight());
			glTexCoord2f(0, 1);		glVertex2f(0, Display.getHeight());
		glEnd();
		Texture.unbind();
		
		spaceButton.render();
		importButton.render();
		infoButton.render();
		
		Text.PIXEL.draw("�Tybau 2016", Display.getWidth() - 150, Display.getHeight() - 20);
	}
}
