package com.tybau.voxel.editor.menu;

/**
 * Created by Tybau on 20/01/2016.
 */
public abstract class Menu
{
	public abstract void update();
	public abstract void render();
	public abstract void renderGui();
}
