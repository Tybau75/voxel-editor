package com.tybau.voxel.editor.menu;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import org.lwjgl.opengl.Display;

import com.tybau.voxel.Editor;
import com.tybau.voxel.maths.Vec2;
import com.tybau.voxel.render.Texture;
import com.tybau.voxel.render.guis.Button;
import com.tybau.voxel.render.guis.Text;

/**
 * Created by Tybau on 23/01/2016.
 */
public class InfoMenu extends Menu
{
	private Button returnButton;
	
	public InfoMenu()
	{
		returnButton = new Button("return", new Vec2(Display.getWidth() - 150, Display.getHeight() - 30));
	}
	
	public void update()
	{
		if(returnButton.isPressed()) Editor.CURRENT_MENU = new MainMenu();
	}

	public void render()
	{
		
	}

	public void renderGui()
	{
		MainMenu.BACKGROUND.bind();
		glBegin(GL_QUADS);
			glTexCoord2f(0, 0);		glVertex2f(0, 0);
			glTexCoord2f(1, 0);		glVertex2f(Display.getWidth(), 0);
			glTexCoord2f(1, 1);		glVertex2f(Display.getWidth(), Display.getHeight());
			glTexCoord2f(0, 1);		glVertex2f(0, Display.getHeight());
		glEnd();
		Texture.unbind();
		
		Text.PIXEL.draw("Controls:\n  - (zqsd): Move in workspace\n  - Mouse: Camera rotation\n  - tab: Second edit mode", 200, 200);
		
		returnButton.render();
	}

}
