package com.tybau.voxel.maths;

/** 
* Created by Tybau on 18/01/2016. 
*/
public class Vec3
{
	public float x, y, z;

	public Vec3()
	{
		this(0, 0, 0);
	}

	public Vec3(Vec3 v)
	{
		this(v.x, v.y, v.z);
	}

	public Vec3(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public String toString()
	{
		return "(" + x + ", " + y + ", " + z + ")";
	}

	public float length()
	{
		return (float) Math.sqrt(x * x + y * y + z * z);
	}

	public Vec3 normalize()
	{
		x /= length();
		y /= length();
		z /= length();

		return this;
	}

	public Vec3 add(Vec3 v)
	{
		x += v.x;
		y += v.y;
		z += v.z;

		return this;
	}

	public Vec3 sub(Vec3 v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;

		return this;
	}

	public Vec3 mul(Vec3 v)
	{
		x *= v.x;
		y *= v.y;
		z *= v.z;

		return this;
	}

	public Vec3 div(Vec3 v)
	{
		x /= v.x;
		y /= v.y;
		z /= v.z;

		return this;
	}

	public Vec3 add(float v)
	{
		x += v;
		y += v;
		z += v;

		return this;
	}

	public Vec3 sub(float v)
	{
		x -= v;
		y -= v;
		z -= v;

		return this;
	}

	public Vec3 mul(float v)
	{
		x *= v;
		y *= v;
		z *= v;

		return this;
	}

	public Vec3 div(float v)
	{
		x /= v;
		y /= v;
		z /= v;

		return this;
	}
}
