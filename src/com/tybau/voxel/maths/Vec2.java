package com.tybau.voxel.maths;

/** 
* Created by Tybau on 18/01/2016. 
*/
public class Vec2
{
	public float x, y;

	public Vec2()
	{
		this(0, 0);
	}

	public Vec2(Vec2 v)
	{
		this(v.x, v.y);
	}

	public Vec2(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public String toString()
	{
		return "(" + x + ", " + y + ")";
	}

	public float length()
	{
		return (float) Math.sqrt(x * x + y * y);
	}

	public Vec2 normalize()
	{
		x /= length();
		y /= length();

		return this;
	}

	public Vec2 add(Vec2 v)
	{
		x += v.x;
		y += v.y;

		return this;
	}

	public Vec2 sub(Vec2 v)
	{
		x -= v.x;
		y -= v.y;

		return this;
	}

	public Vec2 mul(Vec2 v)
	{
		x *= v.x;
		y *= v.y;

		return this;
	}

	public Vec2 div(Vec2 v)
	{
		x /= v.x;
		y /= v.y;

		return this;
	}

	public Vec2 add(float v)
	{
		x += v;
		y += v;

		return this;
	}

	public Vec2 sub(float v)
	{
		x -= v;
		y -= v;

		return this;
	}

	public Vec2 mul(float v)
	{
		x *= v;
		y *= v;

		return this;
	}

	public Vec2 div(float v)
	{
		x /= v;
		y /= v;

		return this;
	}
}
