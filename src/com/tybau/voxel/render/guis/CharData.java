package com.tybau.voxel.render.guis;

/**
 * Created by Tybau on 19/01/2016.
 */
public class CharData
{
	public int id;
	public double x;
	public double y;
	public double width;
	public double height;
	public double xo;
	public double yo;

	protected CharData(int id, double x, double y, double width, double height, double xo, double yo)
	{
		this.id = id;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.xo = xo;
		this.yo = yo;
	}
}
