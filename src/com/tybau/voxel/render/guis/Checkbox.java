package com.tybau.voxel.render.guis;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import com.tybau.voxel.inputs.MouseState;
import com.tybau.voxel.maths.Vec2;
import com.tybau.voxel.render.Texture;

/**
 * Created by Tybau on 20/01/2016.
 */
public class Checkbox
{
	public static final Texture on = new Texture("guis/check_on.png");
	public static final Texture off = new Texture("guis/check_off.png");
	
	private Vec2 pos;
	private String label;
	
	private boolean checked;

	public Checkbox(Vec2 pos, boolean checked, String label)
	{
		this.pos = pos;
		this.checked = checked;
		this.label = label;
	}

	public boolean isPressed()
	{
		if (isHover() && MouseState.isLeftClickPressed())
		{
			this.checked = !this.checked;
			return true;
		}
		return false;
	}

	private boolean isHover()
	{
		if (Mouse.getX() >= pos.x && Mouse.getX() <= pos.x + 25 && Display.getHeight() - Mouse.getY() >= pos.y
				&& Display.getHeight() - Mouse.getY() <= pos.y + 25)
			return true;
		return false;
	}

	public void render()
	{
		if(this.checked) on.bind();
		else off.bind();
		glBegin(GL_QUADS);
			glTexCoord2f(0, 0);		glVertex2f(pos.x, pos.y);
			glTexCoord2f(1, 0);		glVertex2f(pos.x + 25, pos.y);
			glTexCoord2f(1, 1);		glVertex2f(pos.x + 25, pos.y + 25);
			glTexCoord2f(0, 1);		glVertex2f(pos.x, pos.y + 25);
		glEnd();
		Texture.unbind();
		
		Text.PIXEL.draw(label, pos.x + 30, pos.y + 5);
	}

	public boolean isChecked()
	{
		return checked;
	}

	public void setChecked(boolean checked)
	{
		this.checked = checked;
	}
}
