package com.tybau.voxel.render.guis;

import java.io.*;
import java.util.HashMap;

import com.tybau.voxel.render.Texture;

import static org.lwjgl.opengl.GL11.*;

/**
* Created by Tybau on 19/01/2016.
*/
public class Text
{
	public static final Text PIXEL = new Text("pixel");

	private static final int PADDING = 4;

	private BufferedReader reader;
	private Texture texture;

	private HashMap<String, CharData> data = new HashMap<>();
	private HashMap<String, String> values = new HashMap<>();

	public Text(String font)
	{
		try
		{
			reader = new BufferedReader(
					new InputStreamReader(Text.class.getResourceAsStream("/textures/fonts/" + font + ".fnt")));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		while (bindChar())
			;

		texture = new Texture("fonts/" + font + ".png");
	}

	public boolean bindChar()
	{
		values.clear();
		String line = null;
		try
		{
			line = reader.readLine();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		if (line == null)
			return false;
		if (!line.startsWith("char "))
			return true;

		for (String param : line.split(" "))
		{
			String[] p = param.split("=");
			if (p.length == 2)
			{
				values.put(p[0], p[1]);
			}
		}
		int id = Integer.parseInt(values.get("id"));

		double x = Double.parseDouble(values.get("x"));
		double y = Double.parseDouble(values.get("y"));
		double width = Integer.parseInt(values.get("width"));
		double height = Integer.parseInt(values.get("height"));
		double xo = Double.parseDouble(values.get("xoffset"));
		double yo = Double.parseDouble(values.get("yoffset"));

		data.put(String.valueOf(Character.toChars(id)), new CharData(id, x, y, width, height, xo, yo));

		return true;
	}

	public void draw(String str, float x, float y, float s)
	{
		float xo = x;
		float yo = y;
		for (String c : str.split(""))
		{
			CharData cd = data.get(c);
			if (c.equalsIgnoreCase(" "))
			{
				xo += s * (data.get("i").width - PADDING);
			}
			else if (c.equalsIgnoreCase("\n"))
			{
				yo += (data.get("p").height - PADDING) * s;
				xo = x;
			}
			else
			{
				drawChar(cd, xo, yo, s);
				xo += (cd.width - PADDING) * s;
			}
		}
	}

	public void draw(String str, float x, float y)
	{
		this.draw(str, x, y, 1);
	}

	private void drawChar(CharData cd, float x, float y, float s)
	{
		texture.bind();
		glBegin(GL_QUADS);
		glTexCoord2d((cd.x) / texture.getWidth(), (cd.y) / texture.getHeight());
		glVertex2d(x + cd.xo * s, y + cd.yo * s);

		glTexCoord2d((cd.x + cd.width) / texture.getWidth(), (cd.y) / texture.getHeight());
		glVertex2d(x + s * cd.width + cd.xo * s, y + cd.yo * s);

		glTexCoord2d((cd.x + cd.width) / texture.getWidth(), (cd.y + cd.height) / texture.getHeight());
		glVertex2d(x + s * cd.width + cd.xo * s, y + s * cd.height + cd.yo * s);

		glTexCoord2d((cd.x) / texture.getWidth(), (cd.y + cd.height) / texture.getHeight());
		glVertex2d(x + cd.xo * s, y + s * cd.height + cd.yo * s);
		glEnd();
		Texture.unbind();
	}

	public float getTextWidth(String text, float scale)
	{
		float w = 0;
		for (String c : text.split(""))
		{
			CharData cd = data.get(c);
			w += (cd.width - PADDING) * scale;
		}
		return w;
	}

	public float getTextWidth(String text)
	{
		return this.getTextWidth(text, 1);
	}
}
