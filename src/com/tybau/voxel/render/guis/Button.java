package com.tybau.voxel.render.guis;

import com.tybau.voxel.inputs.MouseState;
import com.tybau.voxel.maths.Vec2;
import com.tybau.voxel.render.Color4;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

/**
 * Created by Tybau on 19/01/2016.
 */
public class Button
{
	private String text;
	private Vec2 pos;

	private Color4 color;
	
	private float w;
	private float h;

	public Button(String text, Vec2 pos)
	{
		this.text = text;
		this.pos = pos;

		this.color = new Color4(0.2f, 0.2f, 0.2f, 0.6f);
		
		w = Text.PIXEL.getTextWidth(text) + 5;
		h = 20;
	}

	public boolean isPressed()
	{
		if (isHover() && MouseState.isLeftClickPressed())
			return true;
		return false;
	}
	
	public boolean isDown()
	{
		if (isHover() && Mouse.isButtonDown(0))
			return true;
		return false;
	}

	private boolean isHover()
	{
		if (Mouse.getX() >= pos.x - 5 && Mouse.getX() <= pos.x + w && Display.getHeight() - Mouse.getY() >= pos.y - 5
				&& Display.getHeight() - Mouse.getY() <= pos.y + h)
		{
			this.color = new Color4(0.5f, 0.5f, 0.5f, 0.6f);
			return true;
		}
		this.color = new Color4(0.2f, 0.2f, 0.2f, 0.6f);
		return false;
	}

	public void render()
	{
		color.bind();
		glBegin(GL_QUADS);
		glVertex2f(pos.x - 5, pos.y - 5);
		glVertex2f(pos.x + w, pos.y - 5);
		glVertex2f(pos.x + w, pos.y + h);
		glVertex2f(pos.x - 5, pos.y + h);
		glEnd();
		Color4.unbind();

		Text.PIXEL.draw(text, pos.x, pos.y);
	}
	
	public float getWidth(){
		return w + 5;
	}
	
	public float getHeight(){
		return h + 5;
	}
}
