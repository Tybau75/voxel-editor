package com.tybau.voxel.render;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import com.tybau.voxel.maths.Vec3;

/**
* Created by Tybau on 18/01/2016.
*/
public class Camera
{

	public static float sensibility = 0.3f;
	public static float moveSpeed = 0.1f;

	float fov;
	float zNear;
	float zFar;

	Vec3 position;
	Vec3 rotation;

	public Camera(Vec3 position)
	{
		this.position = position;
		this.rotation = new Vec3(0, 180, 0);
	}

	public Camera setPerspectiveProjection(float fov, float zNear, float zFar)
	{
		this.fov = fov;
		this.zNear = zNear;
		this.zFar = zFar;

		return this;
	}

	public void getPerspectiveProjection()
	{
		glEnable(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(fov, (float) Display.getWidth() / (float) Display.getHeight(), zNear, zFar);
		glEnable(GL_MODELVIEW);
	}

	public void update()
	{
		glPushAttrib(GL_TRANSFORM_BIT);
			glRotatef(rotation.x, 1, 0, 0);
			glRotatef(rotation.y, 0, 1, 0);
			glRotatef(rotation.z, 0, 0, 1);
			glTranslatef(-position.x, -position.y, -position.z);
		glPopMatrix();
	}

	public void input()
	{
		if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) Mouse.setGrabbed(false);
		
		rotation.x -= Mouse.getDY() * sensibility;
		rotation.y += Mouse.getDX() * sensibility;

		if (rotation.x > 90)
			rotation.x = 90;
		if (rotation.x < -90)
			rotation.x = -90;

		if (Keyboard.isKeyDown(Keyboard.KEY_Z))
		{
			position.add(getDefaultDirection().mul(moveSpeed));
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_S))
		{
			position.add(getBackward().mul(moveSpeed));
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_Q))
		{
			position.add(getLeft().mul(moveSpeed));
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_D))
		{
			position.add(getRight().mul(moveSpeed));
		}

		if (Keyboard.isKeyDown(Keyboard.KEY_SPACE))
		{
			position.add(new Vec3(0, 1, 0).mul(moveSpeed));
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))
		{
			position.add(new Vec3(0, -1, 0).mul(moveSpeed));
		}
	}

	public Vec3 getDefaultDirection()
	{
		Vec3 r = new Vec3();

		Vec3 rot = new Vec3(rotation);

		float cosY = (float) Math.cos(Math.toRadians(rot.y - 90));
		float sinY = (float) Math.sin(Math.toRadians(rot.y - 90));
		float cosP = (float) Math.cos(Math.toRadians(-rot.x));
		float sinP = (float) Math.sin(Math.toRadians(-rot.x));

		r.x =cosY * cosP;
		r.y = sinP;
		r.z =sinY * cosP;

		r.normalize();

		return new Vec3(r);
	}

	public Vec3 getBackward()
	{
		return new Vec3(getDefaultDirection().mul(-1));
	}

	public Vec3 getRight()
	{
		Vec3 rot = new Vec3(rotation);

		Vec3 r = new Vec3();
		r.x = (float) Math.cos(Math.toRadians(rot.y));
		r.z = (float) Math.sin(Math.toRadians(rot.y));
		r.normalize();

		return new Vec3(r);
	}

	public Vec3 getLeft()
	{
		return new Vec3(getRight().mul(-1));
	}

	public Vec3 getPos()
	{
		return position;
	}

	public void setPos(Vec3 position)
	{
		this.position = position;
	}

	public Vec3 getRot()
	{
		return rotation;
	}

	public void setRot(Vec3 rotation)
	{
		this.rotation = rotation;
	}
}
