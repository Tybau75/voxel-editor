package com.tybau.voxel.render;

import static org.lwjgl.opengl.GL11.glColor4f;

/**
 * Created by Tybau on 18/01/2016.
 */
public class Color4
{
	public static final Color4 WHITE = new Color4(1, 1, 1);
	public static final Color4 GREEN = new Color4(0, 1, 0);
	public static final Color4 RED = new Color4(1, 0, 0);
	public static final Color4 BLUE = new Color4(0, 0, 1);
	public static final Color4 DARK_GREEN = new Color4(0.1f, 0.5f, 0.2f);

	public float r, g, b, a;

	public Color4(float r, float g, float b, float a)
	{
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public Color4(float r, float g, float b)
	{
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = 1;
	}
	
	public Color4(Color4 c)
	{
		this.r = c.r;
		this.g = c.g;
		this.b = c.b;
		this.a = c.a;
	}
	
	public Color4 mul(float v)
	{
		this.r *= v;
		this.g *= v;
		this.b *= v;
		this.a *= v;
		
		return this;
	}
	
	public void bind(){
		glColor4f(r, g, b, a);
	}
	
	public void bind(float v){
		glColor4f(r * v, g * v, b * v, a);
	}
	
	public static void unbind(){
		glColor4f(1, 1, 1, 1);
	}
}
