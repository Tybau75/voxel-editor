package com.tybau.voxel;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

/** 
* Created by Tybau on 18/01/2016. 
*/
public class Main
{
	public static final int WIDTH = 1280;
	public static final int HEIGHT = 720;
	public static final String TITLE = "Moteur Voxel";
	
	Editor editor;
	
	public static void main(String[] args)
	{
		new Main().start();
	}
	
	public void start()
	{
		try
		{
			Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
			Display.setTitle(TITLE);
			Display.create();
		}
		catch (LWJGLException e)
		{
			e.printStackTrace();
		}
		
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		editor = new Editor();
		
		loop();
	}
	
	long lastTick = System.nanoTime();
	long lastSecond = System.nanoTime();
	int tps = 0;
	int fps = 0;
	
	public void loop()
	{
		editor.init();
		
		while (!Display.isCloseRequested())
		{
			if(lastTick + 1000000000 / 60 <= System.nanoTime())
			{
				editor.update();
				tps++;
				lastTick += 1000000000 / 60;
			}
			
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glClearColor(0.1f, 0.4f, 0.9f, 1.0f);
			
			enable3D();
			editor.render();
			disable3D();
			editor.renderGUI();
			
			Display.update();
			fps++;
			
			if(lastSecond + 1000000000 <= System.nanoTime())
			{
				Display.setTitle(TITLE + "     |     TPS = " + tps + "   FPS = " + fps);
				tps = 0;
				fps = 0;
				lastSecond += 1000000000;
			}
		}
		
		Display.destroy();
		System.exit(0);
	}
	
	private void enable3D()
	{
		glViewport(0, 0, WIDTH, HEIGHT);
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		
		glLoadIdentity();
		
		glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
		
		glEnable(GL_DEPTH_TEST);
	}
	
	private void disable3D()
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		
		glOrtho(0, WIDTH, HEIGHT, 0, -1, 1);
        
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
		
        glDisable(GL_DEPTH_TEST);
	}
}
