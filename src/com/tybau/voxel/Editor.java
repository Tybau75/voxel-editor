package com.tybau.voxel;

import java.util.Random;

import com.tybau.voxel.editor.menu.MainMenu;
import com.tybau.voxel.editor.menu.Menu;
import com.tybau.voxel.inputs.MouseState;

/** 
* Created by Tybau on 18/01/2016. 
*/
public class Editor
{
	public static final Random RANDOM = new Random();
	
	public static Menu CURRENT_MENU;
	
	public void init()
	{
		CURRENT_MENU = new MainMenu();
	}
	
	public void update()
	{
		CURRENT_MENU.update();
		
		MouseState.update();
	}
	
	public void render()
	{
		CURRENT_MENU.render();
	}
	
	public void renderGUI()
	{
		CURRENT_MENU.renderGui();
	}
}
