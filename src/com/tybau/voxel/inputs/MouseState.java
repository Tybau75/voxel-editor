package com.tybau.voxel.inputs;

import org.lwjgl.input.Mouse;

public class MouseState {

    private static boolean oldLeftClick;
    private static boolean oldRightClick;

    private static boolean leftClick;
    private static boolean rightClick;

    public static void update(){
        oldLeftClick = leftClick;
        oldRightClick = rightClick;
        leftClick = Mouse.isButtonDown(0);
        rightClick = Mouse.isButtonDown(1);
    }

    public static boolean isLeftClickPressed(){
        return leftClick && !oldLeftClick;
    }

    public static boolean isLeftClickReleased(){
        return !leftClick && oldLeftClick;
    }

    public static boolean isRightClickPressed(){
        return rightClick && !oldRightClick;
    }

    public static boolean isRightClickReleased(){
        return !rightClick && oldRightClick;
    }
}
